<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PollingUnit;
use App\Models\VotingHistory;
use App\Models\SessionLevel;




class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['getAllLocalGovt','startUssdSession']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $registeredVoters = PollingUnit::getTotalRegisteredVoters(); 
        $APC_vote = PollingUnit::getTotalAPCVote(); 
        $PDP_vote = PollingUnit::getTotalPDPVote(); 
        return view('admin.dashboard',[
            'registeredVoters' => $registeredVoters,
            'APC_vote' => $APC_vote,
            'PDP_vote' => $PDP_vote,
        ]);
    }

    public function polling()
    {
        $pollingunit = PollingUnit::getAllPollingUnit(); 
        //$wards = PollingUnit::getWard(1);
        //$action = ['Registered Voters', 'APC Vote', 'PDP Vote'];
        //echo $action[0];
        //dd($action);
        return view('admin.PollingUnit.index',['pollingunits' => $pollingunit]);
    }

    public function wardHistory($id)
    {
        $pollingunit = PollingUnit::getSinglePollingUnit($id); 
        $histories   = VotingHistory::getAllWardHistory($pollingunit->pu_code); 
        //dd($pollingunit);
        return view('admin.PollingUnit.votingHistories',['pollingunit' => $pollingunit,'histories' => $histories]);
    }
    
    // APi base one

    public function getAllLocalGovt(Request $request)
    {
        
        $sessionId   = $request["sessionId"];
        $serviceCode = $request["serviceCode"];
        $phoneNumber = $request["phoneNumber"];
        $text        = $request["text"];
        $level       = 0;
        $response    = 'default';

        $current_session = SessionLevel::getSingleSessionLevel($request["sessionId"],$request["phoneNumber"]);
        if($current_session){
            $level = $current_session->level;
            if($request["text"] == 0 || $request['text'] == '*'){
                $level -= 1;
            }
        }

        

        switch ($level) {
            case 0:
                $response = self::oldlevel0($response);
                $level += 1;
                $levelname   = 'localGvt';
                SessionLevel::save_session_level($request,$level,$request["text"],$levelname);
                break;
            case 1:
                $response = self::oldlevel1($request,$response, $request["text"]);
                $level += 1;
                $levelname   = 'ward';
                if($request["text"] != 0 || $request['text'] != '*'){
                    SessionLevel::update_session_level($request,$level,$request["text"],$levelname);
                }
                break;    
            default:
                $response = "END Thank you for updating us, We look forward to more update";
                break;
        }


        
        
        echo $response;
    }

    public function oldlevel0($response)
    {
        $count       = 1;
        $pollingunits = PollingUnit::getLocalGovt(); 
        $response     = "CON       Welcome To Show Room\n";
        $response    .= "Please Select Your Local Goverment?\n";
        

        foreach($pollingunits as $pollingunit){
            $response .= $count.". ".$pollingunit['local_gvt']."\n";
            $count++;
        }

        return $response;
    }

    public function oldlevel1($request,$response,$localgvt)
    {
        $count       = 1;
        $response     = "CON  Please Select Your Ward?\n";

        if($localgvt == 0){
            $count = 10;
            $current_session = SessionLevel::getSingleSessionLevel($request["sessionId"],$request["phoneNumber"]);
            $wards = PollingUnit::getWard($current_session->valueID); 
            for ($i=9; $i <= count($wards); $i++) { 
                $response .= $i.". ".$wards[$i]."\n";
            }
            $response .= "*. Previous\n";
        }else {
            $wards = PollingUnit::getWard($localgvt); 
            foreach($wards as $ward){
                $response .= $count.". ".$ward['ward']."\n";
                $count++;
                if($count > 9) break;
            }
            $response .= "0. Next"."\n";
        }

        

        

        return $response;
    }

    // API base two

    public function startUssdSession(Request $request)
    {
        
        $sessionId   = $request["sessionId"];
        $serviceCode = $request["serviceCode"];
        $phoneNumber = $request["phoneNumber"];
        $text        = $request["text"];
        $level       = 0;
        $response    = 'default';

        
        $current_session = SessionLevel::getSingleSessionLevel($request["sessionId"],$request["phoneNumber"]);
        if($current_session){
            $level = $current_session->level;
        }

        switch ($level) {
            case 0:
                $response = self::level0($response);
                $level    += 1;
                $levelname = 'PU code';
                SessionLevel::save_session_level($request,$level,$request["text"],$levelname);
                break;
            case 1:
                $response = self::level1($request,$response, $request["text"]);
                $level += 1;
                $levelname   = 'Vote Option';
                SessionLevel::update_session_level($request,$level,null,$request["text"],$levelname);
                break; 
            case 2:
                $text = substr($request['text'], -1);
                $response = self::level2($request,$response,$text);
                $level += 1;
                $levelname   = 'Vote';
                SessionLevel::update_session_level($request,$level,$text,$current_session->pu_code,$levelname);
                break;   
            case 3:
                $textArray = explode('*', $request['text']);
                $text = trim(end($textArray));
                $response = self::level3($request,$response,$text);
                $level += 1;
                $levelname   = 'Thank You';
                SessionLevel::update_session_level($request,$level,$text,$current_session->pu_code,$levelname);
                break;            
            default:
                $response = "END Thank you for updating us, We look forward to more update";
                break;
        }


        
        
        echo $response;
    }

    public function level0($response)
    {
        $response     = "CON       Welcome To Situation Room \n";
        $response    .= "Please Enter Your Polling Unit code ?\n";
        return $response;
    }

    public function level1($request,$response)
    {
        $pollingunit = PollingUnit::getSinglePollingUnitByPUCode($request['text']);
        $response     = "CON       Please Select An Option\n";
        $response    .= $pollingunit->local_gvt." LG\n";
        $response    .= $pollingunit->ward." Ward\n";
        $response    .= $pollingunit->location."\n";
        $response    .= "PU Code: ".$pollingunit->pu_code."\n";
        $response    .= "\n";
        $response .= "1. Registered Voters \n";
        $response .= "2. APC Vote \n";
        $response .= "3. PDP Vote \n";
        return $response;
    }

    public function level2($request,$response,$text)
    {
        $action = ['Registered Voters', 'APC Vote', 'PDP Vote'];
        $current_session = SessionLevel::getSingleSessionLevel($request["sessionId"],$request["phoneNumber"]);
        $pollingunit   = PollingUnit::getSinglePollingUnitByPUCode($current_session->pu_code);
        $actionVal     = $text - 1;
        $response     = "CON  Please Enter ".$action[$actionVal]."\n";
        $response     .= "Polling Unit : ".$pollingunit->location." \n";
        $response     .= "Polling Unit Code: ".$current_session->pu_code." \n";
        $response     .= "\n";
        return $response;
    }

    public function level3($request,$response,$text)
    {
        $action = ['Registered Voters', 'APC Vote', 'PDP Vote'];
        $columns = ['registered_voters', 'dora_vote', 'pdp_vote'];
        $current_session = SessionLevel::getSingleSessionLevel($request["sessionId"],$request["phoneNumber"]);
        $pollingunit = PollingUnit::getSinglePollingUnitByPUCode($current_session->pu_code);
        $actionVal = $current_session->valueID - 1;
        self::saveVote($actionVal, $text,$current_session->pu_code);
        $column = $columns[$actionVal];
        $response     = "END Thank You \n";
        $response     .= $text.' '.$action[$actionVal]." has been saved \n";
        return $response;
    }


    //31/09/02/004*1*34556


    public function saveVote($type,$text,$pu_code)
    {
        switch ($type) {
            case 0:
                $registeredVoters = PollingUnit::save_registered_voters($text, $pu_code);
                break;
            case 1:
                $apc = PollingUnit::save_dora_vote($text, $pu_code);
                break; 
            case 2:
                $apc = PollingUnit::save_pdp_vote_units($text, $pu_code);
                break;    
            
            default:
                # code...
                break;
        }

    }

}
