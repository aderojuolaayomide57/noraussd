<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\VotingHistory;



class PollingUnit extends Model
{
    protected $table = 'polling_units';

    public static function getAllPollingUnit(){
        return PollingUnit::all();
    }

    public static function getTotalRegisteredVoters(){
      return PollingUnit::sum('registered_voters');
    }

    public static function getTotalAPCVote(){
      return PollingUnit::sum('dora_vote');
    }

    public static function getTotalPDPVote(){
      return PollingUnit::sum('pdp_vote');
    }

    public static function getSinglePollingUnit($id)
    {
      return PollingUnit::where('id', $id)->first();
    }

    public static function getSinglePollingUnitByPUCode($pu_code)
    {
      return PollingUnit::where('pu_code', $pu_code)->first();
    }
    
    // get all column

    public static function getLocalGovt(){
      return PollingUnit::distinct()->select('local_gvt')->get();
    }

    public static function getWard($parent_id){
      return PollingUnit::where('parent_id', $parent_id)->distinct()->select('ward')->get();
    }

    public static function save_registered_voters($text, $pu_code)
    {
        $update = DB::table('polling_units')
          ->where('pu_code', $pu_code)
          ->update([
            'registered_voters' => $text
            ]);
        if($update){
          return  true;
        } else{
          return false;
        }
    }

    // save apc dora vote by polling units

    public static function save_dora_vote($text, $pu_code)
    {
        $update = DB::table('polling_units')
        ->where('pu_code', $pu_code)
        ->update([
          'dora_vote' => $text
          ]);
        if($update){
          VotingHistory::save_polling_histories($text, $pu_code);
          return true;
        } else{
          return false;
        }
    }

    // save pdp vote by polling units

    public static function save_pdp_vote_units($text, $pu_code)
    {
        $update = DB::table('polling_units')
          ->where('pu_code', $pu_code)
          ->update([
            'pdp_vote' => $text
            ]);
        if($update){
          VotingHistory::save_polling_histories($text, $pu_code);
          return true;
        } else{
          return false;
        }
    }

    



}
