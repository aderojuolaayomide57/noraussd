<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class SessionLevel extends Model
{
    protected $table = 'session_levels';

    public static function getSingleSessionLevel($sessionId,$phonenumber)
    {
      return SessionLevel::where('phonenumber', $phonenumber)->where('sessionID', $sessionId)->first();
    }

    public static function save_session_level($request,$level,$valueID,$levelname)
    {
        $sessionlevel = new sessionlevel();
        $sessionlevel->sessionID = $request['sessionId'];
        $sessionlevel->phonenumber = $request['phoneNumber'];
        $sessionlevel->level = $level;
        $sessionlevel->levelname = $levelname;
        $sessionlevel->pu_code = $valueID;
        if($sessionlevel->save()){
          return true;
        } else{
          return false;
        }
    }


    public static function update_session_level($request,$level,$valueID,$pu_code,$levelname)
    {
        $update = DB::table('session_levels')
          ->where('sessionID', $request['sessionId'])
          ->update([
                    'level' => $level,
                    'levelname' => $levelname,
                    'valueID' => $valueID,
                    'pu_code' => $pu_code,
                ]);
        if($update){
          return true;
        } else{
          return false;
        }
    }

}
