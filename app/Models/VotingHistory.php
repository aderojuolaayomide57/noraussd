<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VotingHistory extends Model
{
  protected $table = 'voting_histories';

    public static function getAllWardHistory($id)
    {
      return VotingHistory::where('pu_code', $id)->get();
    }

    public static function save_polling_histories($text, $pu_code)
    {
        $votinghistory = new VotingHistory();
        $votinghistory->pu_code = $pu_code;
        $votinghistory->vote = $text;
        if($votinghistory->save()){
          return true;
        } else{
          return false;
        }
    }
}
