<nav id="menu" data-search="close">
    <ul>
        <li><a href="{{ url('/home') }}"><span><i class="icon  fa fa-laptop"></i> Dashboard</span></a></li>
        <li><span><i class="icon  fa fa-th-list"></i> Users & Admin</span>
            <ul>
                <li ><a href="{{ url('/adminreg') }}">Add Admin</a></li>
                <li><a href="{{ url('/viewalladmin') }}">View all Admin</a></li>
                <li><a href="{{ url('/admin/listusers') }}"> View All User </a></li>
            </ul>
        </li>
        <li><span><i class="icon  fa fa-th-list"></i> Polling Unit</span>
            <ul>
                <li ><a href="{{ url('/pollingunit') }}">view All Polling Unit</a></li>
            </ul>
        </li>
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                <i class="icon-material-outline-power-settings-new"></i>
                        
                        {{ __('Logout') }}
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                    </form>
            </a>
        </li>
        

    </ul>
</nav>