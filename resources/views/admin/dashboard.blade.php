@extends('layouts.slave')

@section('title')
    Home ||  Admin
@endsection

@section('content')

    <div id="main">

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Library</a></li>
            <li class="active">Data</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">
                <div class="col-md-12">

                    <section class="panel bg-inverse">
                        <div class="tabbable">
                            <ul class="nav nav-tabs chart-change">
                                <li class="active"><a href="javascript:void(0)" data-change-type="bars" data-for-id="#stack-chart"><i class="fa fa-bar-chart-o"></i> &nbsp; Bars Chart</a></li>
                                <li ><a href="javascript:void(0)" data-change-type="lines" data-for-id="#stack-chart"><i class="fa fa-qrcode"></i> &nbsp; Lines Chart</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active">
                                    <div class="widget-chart chart-dark">
                                        <table id="stack-chart" data-stack="true" class="flot-chart" data-type="bars"  data-yaxis-max="3000" data-tool-tip="show" data-width="100%" data-height="300px"  data-bar-width="0.5" data-tick-color="rgba(0,0,0,0.05)">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th style="color : #3db9af;">APC</th>
                                                <th style="color : red;">PDP</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th>JAN</th>
                                                <td>1000</td>
                                                <td>295</td>
                                            </tr>
                                            <tr>
                                                <th>FEB</th>
                                                <td>145</td>
                                                <td>425</td>
                                            </tr>
                                            <tr>
                                                <th>MAR</th>
                                                <td>758</td>
                                                <td>986</td>
                                            </tr>
                                            <tr>
                                                <th>APR</th>
                                                <td>475</td>
                                                <td>548</td>
                                            </tr>
                                            <tr>
                                                <th>MAY</th>
                                                <td>452</td>
                                                <td>441</td>
                                            </tr>
                                            <tr>
                                                <th>JUN</th>
                                                <td>112</td>
                                                <td>257</td>
                                            </tr>
                                            <tr>
                                                <th>JUL</th>
                                                <td>857</td>
                                                <td>546</td>
                                            </tr>
                                            <tr>
                                                <th>AUG</th>
                                                <td>200</td>
                                                <td>125</td>
                                            </tr>
                                            <tr>
                                                <th>SEP</th>
                                                <td>55</td>
                                                <td>147</td>
                                            </tr>
                                            <tr>
                                                <th>OCT</th>
                                                <td>1900</td>
                                                <td>315</td>
                                            </tr>
                                            <tr>
                                                <th>NOV</th>
                                                <td>578</td>
                                                <td>425</td>
                                            </tr>
                                            <tr>
                                                <th>DEC</th>
                                                <td>1900</td>
                                                <td>865</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-12">

                    <div class="well bg-theme-inverse">
                        <div class="widget-tile">
                            <section>
                                <h5><strong>REGISTERED</strong> Voters </h5>
                                <h2>{{ $registeredVoters }}</h2>
                                <div class="progress progress-xs progress-white progress-over-tile">
                                    <div class="progress-bar  progress-bar-white" aria-valuetransitiongoal="97584" aria-valuemax="300000"></div>
                                </div>
                                <label class="progress-label label-white">32.53% of  viewer target</label>
                            </section>
                            <div class="hold-icon"><i class="fa fa-laptop"></i></div>
                        </div>
                    </div>
                    <div class="well bg-theme-inverse">
                        <div class="widget-tile">
                            <section>
                                <h5><strong>APC </strong>Total Vote </h5>
                                <h2>{{ $APC_vote }}</h2>
                                <div class="progress progress-xs progress-white progress-over-tile">
                                    <div class="progress-bar  progress-bar-white" aria-valuetransitiongoal="97584" aria-valuemax="300000"></div>
                                </div>
                                <label class="progress-label label-white">32.53% of  viewer target</label>
                            </section>
                            <div class="hold-icon"><i class="fa fa-laptop"></i></div>
                        </div>
                    </div>

                    <div class="well bg-theme">
                        <div class="widget-tile">
                            <section>
                                <h5><strong>PDP</strong>Total Vote </h5>
                                <h2>{{ $PDP_vote }}</h2>
                                <div class="progress progress-xs progress-white progress-over-tile">
                                    <div class="progress-bar  progress-bar-white" aria-valuetransitiongoal="8590" aria-valuemax="10000"></div>
                                </div>
                                <label class="progress-label label-white"> Just 1410 member to limit , <a href="#">Upgrade Now</a> </label>
                            </section>
                            <div class="hold-icon"><i class="fa fa-bar-chart-o"></i></div>
                        </div>
                    </div>

                    <section class="panel">
                        <header class="panel-heading no-borders">
                            <label class="color">Custom <strong>Label and Title </strong></label>
                        </header>
                        <div class="widget-chart">
                            <div class="label-flot-custom-title"><span>Total Vote Chart</span></div>
                            <table id="example_pieDonut" class="flot-chart" data-type="pie" data-inner-radius="0.7" data-pie-style="shadow" data-tool-tip="show" data-width="100%" data-height="220px" >
                                <thead>
                                <tr>
                                    <th></th>
                                    <th style="color : #3db9af;">APC</th>
                                    <th style="color : red;">PDP</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th></th>
                                    <td>44</td>
                                    <td>8</td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!-- // widget-chart -->

                        <div class="panel-group" id="accordion">
                            <div class="panel panel-shadow">
                                <header class="panel-heading bg-inverse" style="padding:0 10px; margin:20px">
                                    <a class=" color-white" data-toggle="collapse" data-parent="#accordion" href="#accordionOne">
                                        <i class="collapse-caret fa fa-angle-up"></i> Total Vote Chart
                                    </a>
                                </header>
                                <div id="accordionOne" class="panel-collapse collapse">
                                    <div class="panel-body text-center">
                                        <div class="label-flot-custom" data-flot-id="#example_pieDonut"></div>
                                    </div>
                                    <!-- //panel-body -->
                                </div>
                                <!-- //panel-collapse -->
                            </div>
                            <!-- //panel -->
                        </div>
                        <!-- //panel-group -->

                    </section>

                    
                </div>
            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->

    </div>


@endsection
