<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/16/2018
 * Time: 2:07 AM
 */
?>
@extends('layouts.slave')

@section('title')
    Home | Users
@endsection

@section('content')

    <div id="main">


        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Users</a></li>
            <li class="active">View All Users</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">

                <div class="col-lg-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <h2><strong>Users</strong></h2>
                            @if(Session::has('deletesuccess'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif
                        </header>
                        <div class="panel-tools fully color" align="right"  data-toolscolor="#6CC3A0">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                          <table id="dt-filter-search" class="table" cellspacing="0" width="100%">
                            <thead>
                              <tr>
                                <th class="th-sm">Name
                                </th>
                                <th class="th-sm">Email
                                </th>
                                <th class="th-sm">Country
                                </th>
                                <th class="th-sm">User Type
                                </th>
                                <th class="th-sm">Balance
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                            @if (!empty($users))
                                @foreach ($users as $user)
                                <tr>
                                  <td>{{ $user->firstname.' '.$user->lastname }}</td>
                                  <td>{{ $user->email }}</td>
                                  <td>{{ $user->country }}</td>
                                  <td>{{ $user->usertype }}</td>
                                  <td>{{ $user->balance }}</td>
                                </tr>
                                @endforeach
                            @endif
                              
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>Name
                                </th>
                                <th>Email
                                </th>
                                <th>Country
                                </th>
                                <th>User Type
                                </th>
                                <th>Balance
                                </th>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                    </section>
                </div>

            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->


    </div>
<script>
  $(document).ready(function () {
    $('#dt-filter-search').dataTable({

      initComplete: function () {
        this.api().columns().every( function () {
            var column = this;
            var search = $(`<input class="form-control form-control-sm" type="text" placeholder="Search">`)
                .appendTo( $(column.footer()).empty() )
                .on( 'change input', function () {
                    var val = $(this).val()

                    column
                        .search( val ? val : '', true, false )
                        .draw();
                } );

        } );
      } 
    });
  });
</script>
@endsection
