<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/16/2018
 * Time: 2:07 AM
 */
?>
@extends('layouts.slave')

@section('title')
    Home | Polling Histories
@endsection

@section('content')

    <div id="main">


        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Polling Histories</a></li>
            <li class="active">View Polling Histories</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">

                <div class="col-lg-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <h2><strong>Polling Histories</strong></h2>
                            <br>
                            <h3><strong>Local Government: {{ $pollingunit->local_gvt }}</strong></h3><br>
                            <p><strong>Ward:</strong> {{ $pollingunit->ward }}</p>
                            <p><strong>Location:</strong> {{ $pollingunit->location }}</p>
                            <p><strong>Pu code:</strong> {{ $pollingunit->pu_code }}</p>
                            <p><strong>Registered Voters:</strong> {{ $pollingunit->registered_voters }}</p>
                            <p><strong>PDP Vote:</strong> {{ $pollingunit->pdp_vote }}</p>
                            <p><strong>APC Vote:</strong> {{ $pollingunit->dora_vote }}</p>
                        </header>
                        <div class="panel-tools fully color" align="right"  data-toolscolor="#6CC3A0">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                          
                          <div class="table-responsive">
                            <table class="table table-striped table-hover" data-provide="data-table">
                                <thead>
                                <tr>
                                    <th> Vote </th>
                                    <th> Time </th>
                                    <th> Action</th>
                                </tr>
                                </thead>
                                <tbody align="center">
                                    @if (!empty($histories))
                                      @foreach ($histories as $history)
                                        <tr>
                                          <td>{{ $history->vote}}</td>
                                          <td>{{ $history->created_at}}</td>
                                          <td><a href="{{ url('/wardhistory', ['id' => $history->id ]) }}">View History</a></td>
                                        </tr>
                                      @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </section>
                </div>

            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->


    </div>

@endsection
