<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/16/2018
 * Time: 2:07 AM
 */
?>
@extends('layouts.slave')

@section('title')
    Home | Polling Unit
@endsection

@section('content')

    <div id="main">


        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Polling Unit</a></li>
            <li class="active">View All Polling Unit</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">

                <div class="col-lg-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <h2><strong>Polling units</strong></h2>
                            @if(Session::has('deletesuccess'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif
                        </header>
                        <div class="panel-tools fully color" align="right"  data-toolscolor="#6CC3A0">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                          
                            <div class="table-responsive">
                              <table class="table table-striped table-hover" data-provide="data-table">
                                  <thead>
                                  <tr>
                                      <th> Local Gvt</th>
                                      <th> Ward </th>
                                      <th> Location</th>
                                      <th> Pu Code</th>
                                      <th> Registered Voters</th>
                                      <th> APC Vote</th>
                                      <th> PDP Vote</th>
                                      <th> Action</th>
                                  </tr>
                                  </thead>
                                  <tbody align="center">
                                    @if (!empty($pollingunits))
                                    @foreach ($pollingunits as $pollingunit)
                                    <tr>
                                      <td>{{ $pollingunit->local_gvt}}</td>
                                      <td>{{ $pollingunit->ward }}</td>
                                      <td>{{ $pollingunit->location }}</td>
                                      <td>{{ $pollingunit->pu_code }}</td>
                                      <td>{{ $pollingunit->registered_voters }}</td>
                                      <td>{{ $pollingunit->dora_vote }}</td>
                                      <td>{{ $pollingunit->pdp_vote }}</td>
                                      <td><a class="btn btn-inverse btn-sm" href="{{ url('/wardhistory', ['id' => $pollingunit->id ]) }}"> <i class="fa fa-pencil">View History</a></td>
                                    </tr>
                                    @endforeach
                                @endif
                                  </tbody>
                              </table>
                          </div>
                        </div>
                    </section>
                </div>

            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->


    </div>

@endsection
